#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/epoll.h>

const size_t MAX_BUFF = 1024;
const size_t MAX_CLI = 5;

int main (int argc, char ** argv)
{
	int i = 0;
	int srv_fd = -1;
	int epoll_fd = -1;
	int cli_fd = -1;
	struct sockaddr_in srv_addr;
	struct epoll_event e;
	char buff[MAX_BUFF];
	ssize_t read_cnt = 0;

	memset(&srv_addr, 0, sizeof(srv_addr));
	memset(&buff, 0, MAX_BUFF * sizeof(char));
	memset(&e, 0, sizeof(e));

	srv_fd = socket(AF_INET, SOCK_STREAM, 0);
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(5556);
	srv_addr.sin_addr.s_addr = INADDR_ANY;
	bind(srv_fd, (struct sockaddr*) &srv_addr, sizeof(srv_addr));
	listen(srv_fd, MAX_CLI);

	epoll_fd = epoll_create(MAX_CLI+1);
	e.events = EPOLLIN;
	e.data.fd = srv_fd;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, srv_fd, &e);

	//++++++++++++++++++++++++++++++++++++++++++++++++
	for (;;) {
		i = epoll_wait(epoll_fd, &e, 1, -1);
		if (i > 0) {
			if (e.data.fd == srv_fd) {
				printf("client accepted\n");
				cli_fd = accept(srv_fd, 0, 0);
				e.events = EPOLLIN;
				e.data.fd = cli_fd;
				epoll_ctl(epoll_fd, EPOLL_CTL_ADD, cli_fd, &e);
			}
			else {
				cli_fd = e.data.fd;
				read_cnt = read(cli_fd, (void *) buff, MAX_BUFF);
				if (read_cnt == 0) {
					printf("client gone\n");
					epoll_ctl(epoll_fd, EPOLL_CTL_DEL, cli_fd, 0);
					close(cli_fd);
				}
				else {
					buff[read_cnt] = 0;
					printf("echo request: \'%s\'\n", buff);
					write(cli_fd, (void *) buff, read_cnt);
				}
			}
		}
		memset(&e, 0, sizeof(e));
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++

	close(srv_fd);

	return 0;
}

