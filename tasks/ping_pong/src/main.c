#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void* ping(void *unused);
void* pong(void *unused);

int CNT = 0;
int IS_PING = 1;
pthread_mutex_t MTX = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t COND = PTHREAD_COND_INITIALIZER;

int main(int c, char **v)
{
	pthread_t tid_ping, tid_pong;

	if (c != 2) {
		printf("ERROR: invalid argument cnt!\n");
		return 1;
	}

	CNT = atoi(v[1]);

	if ( (pthread_create(&tid_ping, 0, ping, 0) != 0) ||
		(pthread_create(&tid_pong, 0, pong, 0) != 0) ) {
		printf("ERROR: cannot create thread\n");
		return 1;
	}

	pthread_join(tid_ping, 0);
	pthread_join(tid_pong, 0);
	
	return 0;
}

void* ping(void *unused)
{
	int i;
	for (i = 0; i < CNT; ++i) {
		pthread_mutex_lock(&MTX);
		while (!IS_PING)
			pthread_cond_wait(&COND, &MTX);

		printf("PING\n");
		IS_PING = !IS_PING;
		pthread_cond_signal(&COND);
		pthread_mutex_unlock(&MTX);
	}
	return 0;
}

void* pong(void *unused)
{
	int i;
	for (i = 0; i < CNT; ++i) {
		pthread_mutex_lock(&MTX);
		while (IS_PING)
			pthread_cond_wait(&COND, &MTX);

		printf("\tPONG\n");
		IS_PING = !IS_PING;
		pthread_cond_signal(&COND);
		pthread_mutex_unlock(&MTX);
	}
	return 0;
}

