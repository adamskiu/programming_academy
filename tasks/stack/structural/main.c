#include "stack.h"
#include <stdio.h>

int main(int argc, char **argv)
{
	stack_t *s;
	int i = 0;

	s = init();
	for (; i < 1000; ++i)
		push(s, i);
	
	while (top(s, &i) == 0) {
		printf("popped: %d\n", i);
		pop(s);
	}

	return 0;
}

