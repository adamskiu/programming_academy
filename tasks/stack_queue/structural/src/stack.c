#include "stack.h"
#include "stdlib.h"

struct stack {
	int k;
	struct stack *next;
};

stack_t* init(void)
{
	stack_t *res = malloc(sizeof(stack_t));
	res->next = 0;
	return res;
}

void destroy(stack_t *s)
{
	if (s && s->next)
		destroy(s->next);
	
	if (s)
		free(s);
}

int push(stack_t *s, int k)
{
	int res = -1;
	if (s) {
		stack_t *n = malloc(sizeof(stack_t));
		n->k = k;
		n->next = s->next;
		s->next = n;
		res = 0;
	}
	return 0;
}

int top(stack_t *s, int *k)
{
	int res = -1;
	if (s && k && s->next) {
		*k = s->next->k;
		res = 0;
	}
	return res;
}

int pop(stack_t *s)
{
	int res = -1;
	if (s && s->next) {
		stack_t *n = s->next->next;
		free(s->next);
		s->next = n;
		res = 0;
	}
	return res;
}

int empty(stack_t *s)
{
	return (s && s->next) ? 0 : 1;
}

