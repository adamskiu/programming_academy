typedef struct stack stack_t;

stack_t* init(void);
void destroy(stack_t *s);
int push(stack_t *s, int k);
int top(stack_t *s, int *k);
int pop(stack_t *s);
int empty(stack_t *s);

