#include "stack.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

typedef struct sync_data {
	int run;
	stack_t *stack;
	pthread_mutex_t mtx;
	pthread_cond_t cond;
} sync_data_t;

void* consument(void *sync_data);

int main(int argc, char **argv)
{
	int m = 0;
	int n = 0;
	int i = 0;
	pthread_t *consument_tids;
	sync_data_t sd = {
		.run = 1,
		.stack = init(),
		.mtx = PTHREAD_MUTEX_INITIALIZER,
		.cond = PTHREAD_COND_INITIALIZER
	};
	time_t start_time = 0;

	if (argc != 3) {
		printf("ERROR: invalid argument cnt\n");
		return 0;
	}

	m = atoi(argv[1]);
	n = atoi(argv[2]);

	if ( (m < 1) || (n < 1) ) {
		printf("ERROR: arguments must be > 0\n");
		return 0;
	}

	consument_tids = (pthread_t*) malloc(m * sizeof(pthread_t));
	for (i = 0; i < m; ++i) {
		if (pthread_create(consument_tids + i, 0, consument, (void*)&sd) != 0) {
			printf("Cannot create %d. consument\n", i);
			return -1;
		}
	}

	start_time = time(0);
	srand(start_time);
	do {
		i = rand() % 1000 + 1;
		pthread_mutex_lock(&sd.mtx);
		push(sd.stack, i);
		pthread_cond_signal(&sd.cond);
		pthread_mutex_unlock(&sd.mtx);
	} while ( (time(0) - start_time) < n );

	pthread_mutex_lock(&sd.mtx);
	sd.run = 0;
	pthread_cond_broadcast(&sd.cond);
	pthread_mutex_unlock(&sd.mtx);

	for (i = 0; i < m; ++i) {
		pthread_join(consument_tids[i], 0);
	}

	free(consument_tids);
	destroy(sd.stack);

	return 0;
}

void* consument(void *sync_data)
{
	sync_data_t *sd = (sync_data_t *)sync_data;
	int i;

	while (sd->run) {
		pthread_mutex_lock(&sd->mtx);
		while (sd->run && empty(sd->stack))
			pthread_cond_wait(&sd->cond, &sd->mtx);

		if (sd->run) {
			top(sd->stack, &i);
			pop(sd->stack);
			printf("TID %d poped: %d\n", (int)pthread_self(), i);
		}
		pthread_mutex_unlock(&sd->mtx);
	}

	return 0;
}

